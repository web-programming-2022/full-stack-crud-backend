import { IsNotEmpty, Matches, MinLength } from 'class-validator';

export class CreateUserDto {}

export class CreateProductDto {
  @IsNotEmpty()
  login: string;

  @IsNotEmpty()
  name: string;

  @Matches(/^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{8,}$/)
  @MinLength(8)
  @IsNotEmpty()
  password: string;
}
